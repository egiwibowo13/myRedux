import React, {Component} from 'react'
import { TouchableHighlight, View, Text, StyleSheet } from 'react-native'

import { connect } from 'react-redux'
import { fetchPeopleFromAPI } from './src/redux/modules/people/actions'
import { fetchProductFromAPI } from './src/redux/modules/product/actions'
import FlatList from './src/component/flatList/flatList.js'

let styles

class App extends Component {
  render() {
    const {
    container,
      text,
      button,
      buttonText
  } = styles
    const { products, isFetchings, error, info } = this.props.product;
    const { people, isFetching } = this.props.people;
    return (
      <View style={container}>
        <Text style={text}>Redux Example</Text>
        <TouchableHighlight style={button} onPress={() => this.props.getPeople()}>
          <Text style={buttonText}>Load People</Text>
        </TouchableHighlight>
        <TouchableHighlight style={button} onPress={() => this.props.getProduct()}>
          <Text style={buttonText}>Load Product</Text>
        </TouchableHighlight>
        {
          isFetchings || isFetching ? <Text>Loading</Text> : null
        }
        {
          people.length ? (
            people.map((person, i) => {
              return <View key={i} >
                <Text>Name: {person.name}</Text>
                <Text>Birth Year: {person.birth_year}</Text>
              </View>
            })
          ) : null
        }
        {
          products.length != 0 ? (
            products.product.map((item, i) => {
              {/*alert(JSON.stringify(item))*/ }
              return <View key={i} >
                <Text>Name: {item.name}</Text>
                <Text>Code: {item.code}</Text>
              </View>
            })
          ) : null
        }
        <FlatList />
      </View>
    )
  }
}


styles = StyleSheet.create({
  container: {
    marginTop: 100
  },
  text: {
    textAlign: 'center'
  },
  button: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0b7eff'
  },
  buttonText: {
    color: 'white'
  }
})

function mapStateToProps(state) {
  return {
    people: state.people,
    product: state.myproduct
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getPeople: () => dispatch(fetchPeopleFromAPI()),
    getProduct: () => dispatch(fetchProductFromAPI())
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)


