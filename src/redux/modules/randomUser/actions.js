import { FETCHING_USER, FETCHING_USER_SUCCESS, FETCHING_USER_FAILURE } from './constants'

export function fetchUsersFromAPI(page, seed) {
    return (dispatch) => {
        dispatch(getUsers())
        fetch(`https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`, {
            method: 'GET',
        })
            .then(data => data.json())
            .then(json => {
                dispatch(getUsersSuccess(json.results))
            })
            .catch(err => {
                dispatch(getUsersFailure("error cuy"))})
    }
}

export function getUsers() {
    return {
        type: FETCHING_USER
    }
}

export function getUsersSuccess(data) {
    return {
        type: FETCHING_USER_SUCCESS,
        data : data,
    }
}

export function getUsersFailure(err) {
    return {
        type: FETCHING_USER_FAILURE,
        err
    }
}