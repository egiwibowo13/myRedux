import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator, Image, RefreshControl } from "react-native";

import { connect } from 'react-redux'
import { fetchUsersFromAPI } from '../../redux/modules/randomUser/actions'

class FlatListDemo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            data: [],
            page: 1,
            seed: 1,
            error: null,
            refreshing: false
        };
    }

    componentDidMount() {
        this.props.getUsers(this.state.page, this.state.seed);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.users.isFetchings == false) {
            this.setState({ data: this.state.page === 1 ? nextProps.users.users : [...this.state.data, ...nextProps.users.users] })
        }
    }

    handleRefresh = () => {
        this.setState(
            {
                page: 1,
                seed: this.state.seed + 1,
                refreshing: false
            },
            () => {
                this.props.getUsers(this.state.page, this.state.seed);
            }
        );
    };

    handleLoadMore = () => {
        this.setState(
            {
                page: this.state.page + 1
            },
            () => {
                this.props.getUsers(this.state.page, this.state.seed);

            }
        );
    };

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    };

    renderHeader = () => {
        return <Text> Header </Text>;
    };

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
                {this.props.users.isFetchings && this.state.page == 1 ? <Text>loading</Text> :
                    <FlatList
                    style={{margin : 20}}
                        data={this.state.data}
                        renderItem={({ item }) => (
                            <View>
                                <Text>{`${item.name.first} ${item.name.last}`}</Text>
                                <Text>{item.email}</Text>
                                <Image source={{ uri: item.picture.thumbnail }} />
                            </View>
                        )}
                        keyExtractor={item => item.email}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListHeaderComponent={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={50}
                    />
                }

            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        users: state.users
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getUsers: (page, seed) => { dispatch(fetchUsersFromAPI(page, seed)) }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FlatListDemo)